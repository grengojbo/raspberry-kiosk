A basic set up ansible playbooks/config for making raspberry pi computers
work as full screen kiosk machines, for display stuff.

First initial commit, from cleaned up TeenStreet files.

This probably has a bunch of mistakes in it.

Usage:
======

First you need to initialise this ansible install.  This happens on the computer that you
will be using to adminsitrate the pis. ::

    ./setup.sh

this should set up everything you need in an virtual env (called .virtualenv).

All hosts, and host config is stored in the one ``hosts`` file.

There are a bunch of useful shortcuts here too:

``./a`` runs ansible, with whatever options you specify

``./pb`` runs an ansible playbook.  So ``./pb site.yml``, for instance.

``./cmd`` runs a command on a machine.

``./shell`` opens up a shell on the machine that you specify (only once you install an ansible user)

Before you use any of these, you should look through the commands to see what what they do, and you should have a basic understanding of how ansible works too.

What actually gets installed?
=============================

simple-redirect.py
------------------

a simple redirect local webserver.  You configure it to point at a remote computer, and if it can
reach that machine, it returns a HTTP redirect to that address, and if it cannot reach it,
it returns a reasonable error message about it, and every so often tries again.

i3 window manager
-----------------

very simple, borderless window manager

video player
------------

a very simple script / server is installed into the local user, which can be used to fire local
commands, such as running a video player (rather than trying to embed one in a page).  This can
be easily extended/changed for your own purposes.  There are also some bits to go with this, such as rtmpdump, buffer, etc.  You can remove these if you need.

chromium web browser
--------------------

the open source branch of google's chrome.

unclutter
---------

removes the mouse when not needed

other clobber
-------------

read the roles/pis/tasks/install.yml for more details.

Structure:
==========

There are two "*roles*" defined, in the "roles" directory.

- common
- pis

common
------

defines stuff which is probably going to be common to your whole site,
such as ssh config, issue files, timezone, network interfaces, etc

you'll probably want to change this to suit your own install needs

pis
---

defines things which are probably only going to be useful on a raspberry pi
kiosk computer, such as the i3 window manager, chromium full screen, etc.

so.....
-------

If you're already happy with your computer setups, then you may want to NOT use
the ``common`` role, but only the ``pis`` one.  Simply change the ``pis.yml``
to not include it.

I hope you find this all useful!
