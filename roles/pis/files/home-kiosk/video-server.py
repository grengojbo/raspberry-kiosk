#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' video-server.py - (C) 2013 Daniel Fairhead

    ------------------
    a simple localhost only server, which when it recieves commands 
    (http requests) attempts to start a video player.  Useful for
    launching video players locally if you can't embed them, for instance,
    or modifying to run whatever local software you want.
    ------------------

    simple-redirect is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    simple-redirect is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Marlinespike.  If not, see <http://www.gnu.org/licenses/>.


'''


import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import socket
from os import system

from urllib import urlopen
import BaseHTTPServer
from string import Template

COMMANDSTART='~/startstream 10.10.36.122/oflaDemo/livestream &'
COMMANDSTOP='~/stopstream'

PORT_NUMBER = 7171

URL = False

try:
    execfile('/etc/simple-redirect.conf')
except:
    pass

class RedirectHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_GET(self):
        self.do_HEAD()
        if 'start' in self.path:
            print 'Starting video player!'
            system(COMMANDSTART)
        elif 'stop' in self.path:
            print 'Stopping video player!'
            system(COMMANDSTOP)

    def do_POST(self):
        self.do_GET()
   

if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    server = server_class(('127.0.0.1', PORT_NUMBER), RedirectHandler)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()

